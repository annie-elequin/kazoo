import {Database} from '@nozbe/watermelondb';

import Message from './model/Message'; // ⬅️ You'll import your Models here

import adapter from './adapter';

const database = new Database({
  adapter,
  name: 'kazoodb',
  modelClasses: [
    Message, // ⬅️ You'll add Models to Watermelon here
  ],
  actionsEnabled: true,
});

export default database;
