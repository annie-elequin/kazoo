// model/Post.js
import {Model} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';

export default class Messages extends Model {
  static table = 'messages';
  // static associations = {
  //   users: {type: 'belongs_to', key: 'sender'},
  // };

  @field('sender') sender;
  @field('body') body;
}
