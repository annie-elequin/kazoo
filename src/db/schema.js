import {appSchema, tableSchema} from '@nozbe/watermelondb';

export default appSchema({
  version: 1,
  tables: [
    tableSchema({
      name: 'messages',
      columns: [
        {name: 'sender', type: 'string'},
        {name: 'body', type: 'string'},
      ],
    }),
    // tableSchema({
    //   name: 'users',
    //   columns: [
    //     {name: 'name', type: 'string'},
    //     {name: 'jid', type: 'string', isIndexed: true},
    //   ],
    // }),
  ],
});
