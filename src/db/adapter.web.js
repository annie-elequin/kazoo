import schema from './schema';
import migrations from './migrations';

import LokiJSAdapter from '@nozbe/watermelondb/adapters/lokijs';

const adapter = new LokiJSAdapter({
  schema,
  // (You might want to comment it out for development purposes -- see Migrations documentation)
  migrations,
  useWebWorker: false,
  useIncrementalIndexedDB: true,
  // dbName: 'myapp', // optional db name
  // Optional, but recommended event handlers:
  onIndexedDBVersionChange: () => {
    // database was deleted in another browser tab (user logged out), so we must make sure we delete
    // it in this tab as well
    // if (checkIfUserIsLoggedIn()) {
    //   window.location.reload();
    // }
  },
  // (optional, but recommended)
  onQuotaExceededError: error => {
    // Browser ran out of disk space -- do something about it
  },
  onSetUpError: error => {
    // Database failed to load -- offer the user to reload the app or log out
  },
  extraIncrementalIDBOptions: {
    onDidOverwrite: () => {
      // Called when this adapter is forced to overwrite contents of IndexedDB.
      // This happens if there's another open tab of the same app that's making changes.
      // You might use it as an opportunity to alert user to the potential loss of data
    },
  },
});

export default adapter;
