const {client, xml, jid} = require('@xmpp/client');
const parse = require('@xmpp/xml/lib/parse');

class XmppService {
  private cli: any;

  constructor() {}

  create(
    username: string,
    password: string,
    service = 'kazoo.im',
    otherProps = {},
  ) {
    const c = client({
      service,
      username,
      password,
      ...otherProps,
    });
    this.cli = c;
    this.listen();
  }

  start() {
    if (!this.cli) return;
    this.get().start().catch(console.error);
  }

  get() {
    return this.cli;
  }

  listen() {
    this.get().on('error', err => {
      console.error(err);
    });

    this.get().on('offline', () => {
      console.log('offline');
    });

    this.get().on('stanza', async stanza => {
      if (!stanza.is('message')) return;

      console.log('message: ', stanza);

      // const message = stanza.clone();
      // message.attrs.to = stanza.attrs.from;
      // this.get().send(message);
    });

    this.get().on('online', async address => {
      console.log('online ', address);

      // Makes itself available
      // await this.get().send(xml('presence'));
    });
  }

  fetchArchivedMessages() {
    const msg = parse(
      `<iq type='set' id='annie'><query xmlns='urn:xmpp:mam:2' queryid='f27' /></iq>`,
    );
    xmpp.get().send(msg);
  }
}

const xmpp = new XmppService();
export default xmpp;
