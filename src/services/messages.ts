import database from '../db';

class MessageService {
  addMessage = async (sender, body) => {
    const col = database.collections.get('messages');
    return await col.action(async () => {
      return await col.create(msg => {
        msg.sender = sender;
        msg.body = body;
      });
    });
  };

  getAllMessages = async () => {
    return database.collections.get('messages').query().fetch();
  };
}

const messages = new MessageService();
export default messages;
