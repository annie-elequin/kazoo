const Spacing = {
  xs: 6,
  s: 8,
  m: 12,
  l: 18,
  xl: 24,
};

export {Spacing};
