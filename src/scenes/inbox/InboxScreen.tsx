import {Button, Layout, Text} from '@ui-kitten/components';
import * as React from 'react';
import {useEffect} from 'react';
import {SafeAreaView, View} from 'react-native';
import messages from '../../services/messages';
import xmpp from '../../services/xmpp';
import database from '../../db';
import withObservables from '@nozbe/with-observables';
import {useState} from 'react';

export default function InboxScreen() {
  const [messageList, setMessageList] = useState([]);

  useEffect(() => {
    // xmpp.create('annie', '4p5W5XgFcZ');
    // xmpp.start();
  }, []);

  const newMessage = async () => {
    const col = await database.collections.get('messages');
    await database.action(async () => {
      const newMsg = await col.create(msg => {
        msg.sender = 'annie@kazoo.im';
        msg.body = 'again';
      });
    });
    await getAll();
  };

  const getAll = async () => {
    const col = await database.collections.get('messages');
    const msgs = await col.query().fetch();
    setMessageList(msgs);
  };

  return (
    <Layout>
      <SafeAreaView>
        <Text>hey there</Text>
        <Button onPress={newMessage}>newmessage</Button>
        <Button onPress={getAll}>get em</Button>
        {messageList.map(m => (
          <Message message={m} />
        ))}
      </SafeAreaView>
    </Layout>
  );
}

const BaseMessage = ({message}) => {
  return (
    <View>
      <Text>
        {message.sender}: {message.body}
      </Text>
    </View>
  );
};

const enhance = withObservables(['message'], ({message}) => ({
  message,
}));

const Message = enhance(BaseMessage);
