import {useNavigation} from '@react-navigation/native';
import {Layout, Button, Input, Text} from '@ui-kitten/components';
import React, {useState} from 'react';
import {SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {Spacing} from '../../theme/constants';
import xmpp from '../../services/xmpp';

export default function LoginScreen() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const nav = useNavigation();

  const onLogin = () => {
    xmpp.create(username, password);
    nav.navigate('Inbox');
  };

  return (
    <Layout level="4" style={styles.wrapper}>
      <SafeAreaView>
        <Text
          category="h1"
          style={{alignSelf: 'center', marginVertical: Spacing.l}}>
          Kazoo
        </Text>
        <Input
          value={username}
          onChangeText={setUsername}
          placeholder="Username or JID"
          autoCorrect={false}
          autoCapitalize="none"
          autoFocus
          style={styles.bottomSpace}
        />
        <Input
          value={password}
          onChangeText={setPassword}
          placeholder="Password"
          secureTextEntry
          style={styles.bottomSpace}
          autoCorrect={false}
          autoCapitalize="none"
        />
        <Button onPress={onLogin}>Login</Button>
      </SafeAreaView>
    </Layout>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: Spacing.m,
  },
  bottomSpace: {
    marginBottom: Spacing.m,
  },
});
