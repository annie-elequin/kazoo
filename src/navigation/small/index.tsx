import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from '../../scenes/auth/LoginScreen';
import InboxScreen from '../../scenes/inbox/InboxScreen';

const Stack = createStackNavigator();

export default function SmallNavigator() {
  return (
    <Stack.Navigator headerMode="none">
      {/* <Stack.Screen name="Login" component={LoginScreen} /> */}
      <Stack.Screen name="Inbox" component={InboxScreen} />
    </Stack.Navigator>
  );
}
